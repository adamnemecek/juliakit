//
//  AppDelegate.m
//  JuliaKit
//
//  Created by Adam Nemecek on 12/6/18.
//  Copyright © 2018 Adam Nemecek. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
