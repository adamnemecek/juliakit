//
//  ViewController.m
//  JuliaKit
//
//  Created by Adam Nemecek on 12/6/18.
//  Copyright © 2018 Adam Nemecek. All rights reserved.
//

#import "ViewController.h"

int julia()
{
    /* required: setup the Julia context */
    jl_init();

    /* run Julia commands */
    jl_eval_string("print(sqrt(2.0))");

    /* strongly recommended: notify Julia that the
     program is about to terminate. this allows
     Julia time to cleanup pending write requests
     and run all finalizers
     */
    jl_atexit_hook(0);
    return 0;
}

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    jl_value_t *t = jl_true;
//    NSLog(@"%d", (int)t);
//    jl_value_t *d = jl_false;
//    NSLog(@"%d", (int)d);
    julia();
    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
