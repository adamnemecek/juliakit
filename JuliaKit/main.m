//
//  main.m
//  JuliaKit
//
//  Created by Adam Nemecek on 12/6/18.
//  Copyright © 2018 Adam Nemecek. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
